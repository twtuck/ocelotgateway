using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace ocelotgateway
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                    .ConfigureAppConfiguration((host, config) =>
                    {
                        config.SetBasePath(host.HostingEnvironment.ContentRootPath)
                        .AddJsonFile("ocelot.json", false, false); //with consul integration
                    })
                    .UseStartup<Startup>()
                    .UseSerilog((_, config) =>
                    {
                        config
                            .MinimumLevel.Information()
                            .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                            .Enrich.FromLogContext()
                            .WriteTo.File(@"Logs\gateway-.log", rollingInterval: RollingInterval.Day);
                    });
                });
    }
}
