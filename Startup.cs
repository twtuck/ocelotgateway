using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Cache.CacheManager;
using Ocelot.Provider.Consul;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using Ocelot.Provider.Polly;
using Ocelot.Administration;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Common;

namespace ocelotgateway
{
    public class Startup
    {
        public Startup(IConfiguration config)
        {
            Configuration = config;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //H0w_@re_YooU -> Base64 = SDB3X0ByZV9Zb29V
            // var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("SDB3X0ByZV9Zb29V"));
            // var tokenValidationParameters = new TokenValidationParameters
            // {
            //     ValidateIssuerSigningKey = false, //true,
            //     IssuerSigningKey = signingKey,
            //     ValidateIssuer = false, //true,
            //     ValidIssuer = "test",
            //     ValidateAudience = false,
            //     ValidAudience = "test", //audienceConfig["Aud"],
            //     ValidateLifetime = false, //true,
            //     ClockSkew = TimeSpan.Zero,
            //     RequireExpirationTime = false, //true,
            //     RequireAudience = false,
            //     RequireSignedTokens = false,
            //     ValidateActor = false,
            //     ValidateTokenReplay = false,
            // };


            IdentityModelEventSource.ShowPII = true;

            var authenticationProviderKey = "TestKey";
            Action<IdentityServerAuthenticationOptions> options = o =>
                {
                    o.RequireHttpsMetadata = false;
                    o.Authority = "http://localhost:7000";
                    o.ApiName = "FvMembership";
                    o.SupportedTokens = SupportedTokens.Both;
                    o.ApiSecret = "FvMembershipApiSecret";
                };

            services.AddAuthentication()
                .AddIdentityServerAuthentication(authenticationProviderKey, options);

            services
            .AddCors();
            
            // services.AddAuthentication()
            //     .AddJwtBearer("TestKey", x =>
            //         {
            //             // When validating JWT, this middleware method only needs to call the AuthServer once.
            //             // Thereafter, the JWT will always pass even when the AuthServer is down.
            //             x.RequireHttpsMetadata = false;
            //             // x.TokenValidationParameters = tokenValidationParameters;
            //             x.Audience = "http://localhost:7000/resources";     // ==> "aud"
            //             x.Authority = "http://localhost:7000";              // ==> "iss"
            //         });

            // services.AddAuthentication(x =>
            //     {
            //         x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //         x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //     })
            //     .AddJwtBearer("TestKey", o =>
            //     {
            //         // When validating JWT, this middleware method only needs to call the AuthServer once.
            //         // Thereafter, the JWT will always pass even when the AuthServer is down.
            //         o.RequireHttpsMetadata = false;
            //         o.Authority = "http://localhost:7000";
            //         o.Audience = "http://localhost:7000/resources";
            //         o.TokenValidationParameters = new TokenValidationParameters
            //         {
            //             NameClaimType = JwtClaimTypes.Name,
            //             RoleClaimType = JwtClaimTypes.Role,
            //         };
            //     });

            services
            .AddSwaggerForOcelot(Configuration)
            .AddOcelot()
            .AddPolly()
            .AddConsul()
            .AddCacheManager(x => x.WithDictionaryHandle())
            .AddAdministration("/ocelot/admin", "any_secret_will_do")
            ;

            services.AddConsulConfig(Configuration);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app
            .UseAuthentication()
            .UseCors(b => b
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader())
            .UseConsul("iJooz API Gateway")
            .UseRouting()
            .UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Hello from Ocelot");
                });
            })
            .UseMiddleware<RequestResponseLoggingMiddleware>()
            .UseSwaggerForOcelotUI(opts =>
            {
                opts.ServerOcelot = "localhost:4000";
            });

            await app.UseOcelot();
        }
    }
}
