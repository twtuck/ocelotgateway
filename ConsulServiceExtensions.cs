namespace Common
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Sockets;
    using System.Threading.Tasks;
    using System.Timers;
    using Consul;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting.Server.Features;
    using Microsoft.AspNetCore.Http.Features;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Logging;

    public static class ConsulServiceExtensions
    {
        private static int _currentAttempt;
        private static Timer _timer;

        public static IServiceCollection AddConsulConfig(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IConsulClient, ConsulClient>(p => new ConsulClient(consulConfig =>
            {
                var address = "http://localhost:8500";
                consulConfig.Address = new Uri(address);
            }));
            return services;
        }

        public static IApplicationBuilder UseConsul(this IApplicationBuilder app, string serviceName)
        {
            var logger = app.ApplicationServices.GetRequiredService<ILoggerFactory>().CreateLogger("AppExtensions");
            var consulClient = app.ApplicationServices.GetRequiredService<IConsulClient>();
            var consulAgent = consulClient.Agent;

            if (!(app.Properties["server.Features"] is FeatureCollection features)) return app;

            var addresses = features.Get<IServerAddressesFeature>();
            var address = addresses.Addresses.First();
            Console.WriteLine($"address={address}");

            var uri = new Uri(address);
            var registration = new AgentServiceRegistration
            {
                ID = $"{serviceName}-{uri.Port}",
                Name = serviceName,
                Address = $"{uri.Host}",
                Port = uri.Port,
                Check = new AgentServiceCheck { TTL = TimeSpan.FromSeconds(10) }
            };

            RegisterWithConsul(registration, consulAgent, logger);

            var lifetime = app.ApplicationServices.GetRequiredService<IHostApplicationLifetime>();
            lifetime.ApplicationStopping.Register(() =>
            {
                logger.LogInformation($"Deregistering {registration.ID} from Consul");
                consulAgent.ServiceDeregister(registration.ID).ConfigureAwait(false).GetAwaiter().GetResult();
            });

            return app;
        }

        public static void RegisterWithConsul(AgentServiceRegistration registration, IAgentEndpoint consulAgent, ILogger logger)
        {            
            try
            {
                _currentAttempt++;
                logger.LogInformation($"Registering '{registration.ID}' on Consul, attempt #{_currentAttempt}");
                consulAgent.ServiceDeregister(registration.ID).ConfigureAwait(false).GetAwaiter().GetResult();
                consulAgent.ServiceRegister(registration).ConfigureAwait(false).GetAwaiter().GetResult();

                logger.LogInformation($"'{registration.ID}' successfully registered on Consul");
                // successful registration, reset current attempt count to 0
                _currentAttempt = 0;

                SetupHeartbeats(registration, consulAgent, logger);
            }
            catch (HttpRequestException ex)
            {
                var socketErrorCode = (ex.InnerException as SocketException)?.SocketErrorCode;
                if (socketErrorCode != SocketError.ConnectionRefused)
                    throw;

                Task.Delay(5000).ContinueWith(task => RegisterWithConsul(registration, consulAgent, logger));
            }
        }

        public static void SetupHeartbeats(AgentServiceRegistration registration, IAgentEndpoint consulAgent, ILogger logger)
        {
            logger.LogInformation($"Initializing heartbeats for '${registration.ID}'...");
            
            _timer = new Timer(5000);
            _timer.Elapsed += (object sender, ElapsedEventArgs e) =>
            {
                try
                {
                    consulAgent.PassTTL($"service:{registration.ID}", null).ConfigureAwait(false).GetAwaiter().GetResult();
                }
                catch (HttpRequestException ex)
                {
                    _timer.Stop();
                    var socketErrorCode = (ex.InnerException as SocketException)?.SocketErrorCode;
                    if (socketErrorCode != SocketError.ConnectionRefused)
                        throw;

                    logger.LogInformation($"Connection to Consul failed for '{registration.ID}'. Attempting to reconnect to Consul");
                    Task.Delay(5000).ContinueWith(task => RegisterWithConsul(registration, consulAgent, logger));
                }
            };
            _timer.Start();
        }
    }
}